var CACHE_NAME = 'Statera';
var urlsToCache = [
	'/',
	'/assets/css/app.min.css',
	'/assets/js/app.min.js',
	'/assets/images/map.png',
	'/assets/images/filter.png',
	'/assets/images/float.webp',
	'/assets/images/pixel.gif',
	'/assets/images/sport-1235019_1920.jpg' ,
	'/assets/images/telephone-586266_1920.jpg' ,
	'/assets/images/Medical_supplies_and_equipment_15834977505.jpg',
	'/assets/images/question-mark-1872665_1920.jpg',
	'/assets/fonts/PlayfairDisplay.eot',
	'/assets/fonts/PlayfairDisplay.svg',
	'/assets/fonts/PlayfairDisplay.ttf',
	'/assets/fonts/PlayfairDisplay.woff',
	'/assets/fonts/Roboto.eot',
	'/assets/fonts/Roboto.svg',
	'/assets/fonts/Roboto.ttf',
	'/assets/fonts/Roboto.woff',
	'/assets/fonts/TrajanPro.eot',
	'/assets/fonts/TrajanPro.svg',
	'/assets/fonts/TrajanPro.ttf',
	'/assets/fonts/TrajanPro.woff',
	'/assets/fonts/statera-icons.eot',
	'/assets/fonts/statera-icons.svg',
	'/assets/fonts/statera-icons.ttf',
	'/assets/fonts/statera-icons.woff'
];

self.addEventListener('install', function(event) {
	event.waitUntil(
		caches.open(CACHE_NAME)
			.then(function(cache) {
				console.log('Opened cache');
				return cache.addAll(urlsToCache);
			})
	);
});

self.addEventListener('fetch', function(event) {
	event.respondWith (
		caches.match(event.request)
			.then(function(response) {
				if (response) {
					return response;
				}
				return fetch(event.request);
			})
	);
});