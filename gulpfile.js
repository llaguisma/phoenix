'use strict';

// let spdy = require('spdy'),
//     fs = require('fs')

// let options = {
//     key: fs.readFileSync(__dirname + '/node-http/server.key'),
//     cert: fs.readFileSync(__dirname + '/node-http/server.crt')
// }

// Load plugins
const autoprefixer = require('gulp-autoprefixer'),
    cache = require('gulp-cache'),
    cleanCSS = require('gulp-clean-css'),
    compression = require('compression'),
    concat = require('gulp-concat'),
    connect = require('connect'),
    cookieSession = require('cookie-session'),
    del = require('del'),
    ejs = require('gulp-ejs'),
    finalhandler = require('finalhandler'),
    flatten = require('gulp-flatten'),
    gulp = require('gulp'),
    gulpif = require('gulp-if'),
    http = require('http'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    include = require('gulp-include'),
    jsonld = require('jsonld'),
    livereload = require('gulp-livereload'),
    log = require('fancy-log'),
    minify = require('@node-minify/core'),
    notify = require('gulp-notify'),
    open = require('open'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    serveIndex = require('serve-index'),
    serveStatic = require('serve-static'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    uglifyJS = require('@node-minify/uglify-js'),
    uglifyES = require('@node-minify/uglify-es')
    
// Define paths
var paths = {  

  fonts: {
    src: 'src/**/*.{eot,svg,ttf,woff,woff2}',
    dest: 'dist/assets/fonts'
  },
  images: {
    src: 'src/images/**',
    dest: 'dist/assets/images'
  },
  root: {
    src: 'src/templates/*.{ico,js,json,png}',
    dest: 'dist'
  },
  sass: {
    src: 'src/css/**/*.{scss,sass,css,map}',
    dest: 'dist/assets/css',
    aux: 'maps'
  },
  scripts: {
    src: 'src/js/**/*.js',
    dest: 'dist/assets/js'
  },
  styles: {
    src: ['src/css/*.{scss,sass,css,map}', 'src/vendor/*.{scss,sass,css,map}'],
    dest: 'dist/assets/css'
  },
  templates: {
    src: 'src/templates/*.ejs',
    dest: 'dist'
  },
  vdo: {
    src: 'src/vdo/**',
    dest: 'dist/assets/vdo'
  },
  vendorcss: {
    src: 'src/vendor/**',
    dest: 'dist/assets/css'
  },
  vendorjs: {
    src: 'src/vendor/**',
    dest: 'dist/assets/js'
  },
  watch: {
    src: 'dist/*.html',
    dest: 'dist/assets/**'
  }

};

//Clear cache
function clear() {
  cache.clearAll()
}
exports.clear = clear;

//Clean app directory
function clean() {
  clear()

  return del(['dist/'])
  notify({
    'message': 'CLEAN task complete',
    'onLast': true
  });
}
exports.clean = clean;

function styles() {
  return gulp.src(paths.styles.src)
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: [
        process.cwd() + 'src/css/partials',
        process.cwd() + 'src/vendor/bootstrap/scss',
        process.cwd() + 'src/vendor/bootstrap/scss/mixins',
        process.cwd() + 'src/vendor/bootstrap/scss/utilities',
        process.cwd() + 'src/vendor/bootstrap/scss/vendor'
      ],
      errLogToConsole: true
    }).on('error', sass.logError))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(livereload())
    .pipe(notify({
      'message': 'STYLES task complete',
      'onLast': true
    }))
}
exports.styles = styles;

function sassquatch() {
  return gulp.src(paths.sass.src)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write(paths.sass.aux))
    .pipe(gulp.dest(paths.sass.dest))
    .pipe(livereload())
    .pipe(notify({
      'message': 'SASS:Sourcemaps task complete',
      'onLast': true
    }))
}
exports.sassquatch = sassquatch;

function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    .pipe(concat('app.js'))
    .pipe(include())
    .pipe(rename({
      basename: 'app',
      suffix: '.min'
    }))
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(livereload())
    .pipe(notify({
      'message': 'SCRIPTS task complete',
      'onLast': true
    }))
}
exports.scripts = scripts;

function root() {
  return gulp.src(paths.root.src)
  .pipe(gulp.dest(paths.root.dest))
  .pipe(notify({
    'message': 'ROOT task complete',
    'onLast': true
  }))
}
exports.root = root;

function images() {
  return gulp.src(paths.images.src)
  .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.mozjpeg({quality: 75, progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({
        plugins: [
            {removeViewBox: true},
            {cleanupIDs: false}
        ]
    })
  ]))

  // .pipe(cache(imagemin([
  //   imagemin.gifsicle({interlaced: true}),
  //   imagemin.mozjpeg({quality: 75, progressive: true, use: [pngquant()]}),
  //   imagemin.optipng({optimizationLevel: 5}),
  //   imagemin.svgo({
  //       plugins: [
  //           {removeViewBox: true},
  //           {cleanupIDs: false}
  //       ]
  //   })
  // ],
  // {
  //   verbose: true
  // })))
  .pipe(gulp.dest(paths.images.dest))
  .pipe(livereload())
  .pipe(notify({
    'message': 'IMAGES task complete',
    'onLast': true
  }))
}
exports.images = images;

function vdo() {
  return gulp.src(paths.vdo.src)
  .pipe(gulp.dest(paths.vdo.dest))
  .pipe(livereload())
  .pipe(notify({
    'message': 'VDO task complete',
    'onLast': true
  }))
}
exports.vdo = vdo;

function vendorcss() {
  return gulp.src(paths.vendorcss.src +'/**/*.{css,map}')
    .pipe(gulp.dest(paths.vendorcss.dest))
    .pipe(livereload())
    .pipe(notify({
      'message': 'VCSS task complete',
      'onLast': true
    }))
}
exports.vendorcss = vendorcss;

function vendorjs() {
  return gulp.src(paths.vendorjs.src +'/**/*.js')
    .pipe(gulp.dest(paths.vendorjs.dest))
    .pipe(notify({
      'message': 'VJS task complete',
      'onLast': true
    }))
}

function templates() {
  return gulp.src(paths.templates.src)
  .pipe(ejs().on('error', log))
  .pipe(rename({ extname: '.html' }))
  .pipe(gulp.dest(paths.templates.dest))
  .pipe(livereload())
  .pipe(notify({
    'message': 'TEMPLATES task complete',
    'onLast': true
  }));
}
exports.templates = templates;

function fonts() {
  return gulp.src(paths.fonts.src)
  .pipe(flatten())
  .pipe(gulp.dest(paths.fonts.dest))
  .pipe(livereload())
  .pipe(notify({
    'message': 'FONTS task complete',
    'onLast': true
  }))
}
exports.fonts = fonts;

// Build
var build = gulp.series(clean, gulp.parallel(root, styles, vendorcss, vendorjs, scripts, images, templates, fonts, vdo));
exports.build = build;

exports.default = build;

// // Setup connect server
function connection() {
  var app = connect()
  app.use(livereload({ port: 35729 }))
  app.use(compression());
  app.use(cookieSession({
    keys: ['secret1', 'secret2']
  }));
  app.use(serveStatic('./dist'), {'index': ['index.html', 'index.htm']})
  app.use(serveIndex('./dist'))

  var port = 9000
  
  http.createServer(app).listen(port)

};
exports.connection = connection;

function serve() {
  const open = require('open');
  (async () => {
    open('http://localhost:9000', {app: ['google chrome', '--incognito']});
  })();
}
exports.serve = serve;

// Watch
function watch() {
  connection()
  serve()

  gulp.watch('src/css/**/*.{scss,sass,css,map}', styles);
  gulp.watch('src/js/**/*.js', scripts);
  gulp.watch('src/templates/**/*.{ejs,js,json,png}', templates);
  gulp.watch('src/vendor/**/*.{css,js}');
  gulp.watch('src/vdo/*.{jpg,mp4,webm}', vdo);
  gulp.watch(paths.fonts.src, fonts);

  // Create LiveReload server
  livereload({ start: true });
  livereload.listen();

  gulp.watch([paths.scripts.dest, paths.scripts.src]).on('change', function(file) {
    livereload.changed(file.paths);
  });

  notify({
    'message': 'Watching for file changes ...',
    'onLast': true
  });
}
exports.watch = watch;