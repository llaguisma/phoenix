//home button
$('.menulogo').on('click', function(){
	$('html, body').animate({
		scrollTop: ($('html').offset().top)
	},500);
});

//dark-mode button
$('.dark-mode').on('click', function(){
	$(this).toggleClass('enabled');
	$('html').toggleClass('dark-knight');
});

//season-mode button
$(document).on('click', 'button.springtime', function(){
	$(this).addClass('summertime').removeClass('springtime');
	$('html').addClass('summer').removeClass('spring');
	updateBackground("summer", summer);
});

$(document).on('click', 'button.summertime', function(){
	$(this).addClass('falltime').removeClass('summertime');
	$('html').addClass('fall').removeClass('summer');
	updateBackground("fall", fall);
});

$(document).on('click', 'button.falltime', function(){
	$(this).addClass('wintertime').removeClass('falltime');
	$('html').addClass('winter').removeClass('fall');
	updateBackground("winter", winter);
});

$(document).on('click', 'button.wintertime', function(){
	$(this).addClass('springtime').removeClass('wintertime');
	$('html').addClass('spring').removeClass('winter');
	updateBackground("spring", spring);
});

//about button
$('.about-button').on('click', function(){
	$('.about .article-footer').addClass('about--gigante');
	$('.about').addClass('about--resize');
	$('.about .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.about').offset().top)
	},500);
});

//about ian's profile
$('.ian-button').on('click', function(){
	// change size of item via class
	$('.about .article-footer').addClass('about--gigante');
	$('.about').addClass('about--resize');
	$('.about .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.about').offset().top)
	},500);
});

//about staff
$('.staff-button').on('click', function(){
	// change size of item via class
	$('.about .article-footer').addClass('about--gigante');
	$('.about').addClass('about--resize');
	$('.about .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.about').offset().top)
	},500);
});

//about read more toggle
$('.about .article-footer').on('click', function(){
	$('.about .article-footer').toggleClass('about--gigante');
	$('.about').toggleClass('about--resize');
	$('.about .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('.about').offset().top)
	},500);
});

//booking button
$('.bookme-button, .bookme-link, .bookme-float-button, .bookme-cta-button').on('click', function(){
	$('.booking .article-footer').addClass('booking--gigante');
	$('.booking').addClass('booking--resize');
	$('.booking .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.booking').offset().top)
	},500);
});

//booking read more toggle
$('.booking .article-footer').on('click', function(){
	$('.booking .article-footer').toggleClass('booking--gigante');
	$('.booking').toggleClass('booking--resize');
	$('.booking .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('.booking').offset().top)
	},500);
});

//treatments button
$('.treatment-button').on('click', function(){
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.treatment').offset().top)
	},500);
});

//treatments-services read more toggle
$('.treatment .article-footer').on('click', function(){
	$('.treatment .article-footer').toggleClass('treatment--gigante');
	$('.treatment').toggleClass('treatment--resize');
	$('.treatment .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('.treatment').offset().top)
	},500);
});

$('.depression-button').on('click', function(){
	// change size of item via class
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#depression').offset().top)
	},500);
});

$('.vit-button').on('click', function(){
	// change size of item via class
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#infusion').offset().top)
	},500);
});

$('.ptsd-button').on('click', function(){
	// change size of item via class
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#ptsd').offset().top)
	},500);
});

$('.injection-button').on('click', function(){
	// change size of item via class
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#injection').offset().top)
	},500);
});

$('.pain-button').on('click', function(){
	// change size of item via class
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#pain').offset().top)
	},500);
});

$('.iv-button').on('click', function(){
	// change size of item via class
	$('.treatment .article-footer').addClass('treatment--gigante');
	$('.treatment').addClass('treatment--resize');
	$('.treatment .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#iv').offset().top)
	},500);
});

//finance button
$('.finance-button').on('click', function(){
	$('.finance .article-footer').addClass('finance--gigante');
	$('.finance').addClass('finance--resize');
	$('.finance .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.finance').offset().top)
	},500);
});

//finance read more toggle
$('.finance .article-footer').on('click', function(){
	$('.finance .article-footer').toggleClass('finance--gigante');
	$('.finance').toggleClass('finance--resize');
	$('.finance .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('.finance').offset().top)
	},500);
});

//questions button
$('.questions-button').on('click', function(){
	$('.questions .article-footer').addClass('questions--gigante');
	$('.questions').addClass('questions--resize');
	$('.questions .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.questions').offset().top)
	},500);
});

//questions read more toggle
$('.questions .article-footer').on('click', function(){
	$('.questions .article-footer').toggleClass('questions--gigante');
	$('.questions').toggleClass('questions--resize');
	$('.questions .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('.questions').offset().top)
	},500);
});

//providers button
$('.providers-button').on('click', function(){
	$('.providers .article-footer').addClass('providers--gigante');
	$('.providers').addClass('providers--resize');
	$('.providers .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('#providers').offset().top)
	},500);
});

//providers read more toggle
$('.providers .article-footer').on('click', function(){
	$('.providers .article-footer').toggleClass('providers--gigante');
	$('.providers').toggleClass('providers--resize');
	$('.providers .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('#providers').offset().top)
	},500);
});

//contact button
$('.contact-button').on('click', function(){
	$('.contact .article-footer').addClass('contact--gigante');
	$('.contact').addClass('contact--resize');
	$('.contact .read').addClass('more');
	$('html, body').animate({
		scrollTop: ($('.contact').offset().top)
	},500);
});

//contact read more toggle
$('.contact .article-footer, .jumptomap').on('click', function(){
	$('.contact .article-footer').toggleClass('contact--gigante');
	$('.contact').toggleClass('contact--resize');
	$('.contact .read').toggleClass('more');
	$('html, body').animate({
		scrollTop: ($('.googlemap').offset().top)
	},500);
});

//career button
$('.careers-button').on('click', function(){
	//set status
	$('aside').removeClass('inactive');
	$('aside').addClass('active');
	//set new height hidden inner container
	$('aside').addClass('open-careers-context');
	//set new width to open aside
	$('aside').addClass('careers--resize');
	//Reveal content
	$('.careers--resize .career-content').addClass('display--content');
	//Set height for scroll function
	setHeightMode();
	//Mask background
	$('.mask').addClass('display-mask');
	//Reset z-index
	$('.mask').removeClass('z-index-4');
	//Disable scroll
	$('body').addClass('disable-scroll');
	//Reset menu
	$('.hamburger').toggleClass('collapsed');
});

//career read more toggle
$('aside .close-button').on('click', function(){
	//reset status
	$('aside').addClass('inactive');
	$('aside').removeClass('active');
	//reset width to close aside
	$('aside').removeClass('careers--resize');
	//reset height hidden inner container
	$('aside').removeClass('open-careers-context');
	//Hide content
	$('.career-content').removeClass('display--content');
	//Unmask background
	$('.mask').removeClass('display-mask');
	//Re-enable scroll
	$('body').removeClass('disable-scroll');
});

//feedback button
$('.feedback-button').on('click', function(){
	//set status
	$('aside').removeClass('inactive');
	$('aside').addClass('active');
	//set new height hidden inner container
	$('aside').addClass('open-feedback-context');
	//set new width to open aside
	$('aside').addClass('feedback--resize');
	//Reveal content
	$('.feedback--resize .feedback-content').addClass('display--content');
	//Set height for scroll function
	setHeightMode();
	//Mask background
	$('.mask').addClass('display-mask');
	//Reset z-index
	$('.mask').removeClass('z-index-4');
	//Disable scroll
	$('body').addClass('disable-scroll');
});

//feedback read more toggle
$('aside .close-button').on('click', function(){
	//reset status
	$('aside').addClass('inactive');
	$('aside').removeClass('active');
	//reset width to close aside
	$('aside').removeClass('feedback--resize');
	//reset height hidden inner container
	$('aside').removeClass('open-feedback-context');
	//Hide content
	$('.feedback-content').removeClass('display--content');
	//Unmask background
	$('.mask').removeClass('display-mask');
	//Re-enable scroll
	$('body').removeClass('disable-scroll');
});

//hamburger menu open
$('.hamburger').on('click', function(){
	//Mask background
	$(this).toggleClass('collapsed');
	$(this).removeClass('hamburger-focus');
	$('.mask').toggleClass('display-mask');
	$('.mask').toggleClass('z-index-4');
	//Re-enable scroll
	$('body').toggleClass('disable-scroll');
	//Hide CTA-tray
	($('.cta-tray').hasClass('fadeOutLeft')) ? $('.cta-tray').css('display','none') : $('.cta-tray').toggleClass('fadeInLeft fadeOutLeft');
});

//hamburger menu links reset
$('.nav-link.about-us,.nav-link.contact-us,.nav-link.booking-form,.nav-link.treatment-options,.nav-link.finance-help,.nav-link.faqs,.nav-link.providers').on('click', function(){
	$('body').removeClass('disable-scroll');
	$('.mask').removeClass('display-mask');
	$('.mask').removeClass('z-index-4');
	//Reset menu
	$('.hamburger').toggleClass('collapsed');
});
