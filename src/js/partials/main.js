'use strict';

var FUSE = FUSE || {};

(function($) {

	var console = window.console || {
		log: function () { }
	};

	FUSE.Events = {};

	FUSE.EventManager = (function() {
		return {
				subscribe: function (event, fn) {
				$(this).unbind(event, fn).bind(event, fn);
			},
				resign: function (event, fn) {
				$(this).unbind(event, fn);
			},
				publish: function (event) {
				$(this).trigger(event);
			}
		};
	}());

	FUSE.PageManager = (function() {
		var Page;

		Page = (function(ref) {
			var self = {};

			self.init = function() {};

			return self;
		})();

		var initialised = false;

		return {
			init: function() {
				if (initialised) {
					return;
				}
				initialised = true;
				Page.init();
			},
			Page: Page
		}
	})();
})(jQuery);

$(document).on('ready', FUSE.PageManager.init);

//retrieve querystring parameters
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");

	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){return pair[1];}
	}
	return(false);
}
//enable logs
var debug = getQueryVariable("debug");

//return booking from confirm message
var iamMode = getQueryVariable("mode");
var iamThankyou = getQueryVariable("thankyou");

(iamMode == "thankyou") ? $('.booking').addClass("thankyou") : null;

$(function(){
	//determine client browser append hook
	if(navigator.userAgent.match(/Trident.*rv\:11\./)) {
		$('html').addClass('ie11');
	}
	if(navigator.userAgent.match(/Trident\/6\./)) {
		$('html').addClass('ie10');
	}
	if(navigator.userAgent.match(/Trident\/5\./)) {
		$('html').addClass('ie9');
	}

	var didScroll = false;
	window.onscroll = doThisStuffOnScroll;
	$(window).scroll(example);
	
	function example(){
		(debug) ? console.log("tempScrollTop pos."+ $(window).scrollTop()) : null;

		//calculate object co-ords are within viewport is true/false
		$.fn.isInViewport = function(){
			var elementTop = $(this).offset().top;
			var elementBottom = elementTop + $(this).outerHeight();
			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + $(window).height();
			return elementBottom > viewportTop && elementTop < viewportBottom;
		};

		//handle primary-cta display excluding target section
		($('.booking, .about').isInViewport()) ? $('.navbar-nav .menubutton, .cta-tray').addClass('fadeOutLeft').removeClass('fadeInLeft').hide() : $('.navbar-nav .menubutton, .cta-tray').removeClass('fadeOutLeft').addClass('fadeInLeft').show();

		//handle menubar logo display
		($(window).scrollTop() <= '214') ? $('.navbar-nav .menulogo').hide() : $('.navbar-nav .menulogo').show();

		if ($(window).scrollTop() == '0') {
			$('.navbar').removeClass('scroll-past-0');
		} else {
			$('.navbar').addClass('scroll-past-0');
		}
	}

	function doThisStuffOnScroll() {
		didScroll = true;
	}

	setInterval(function() {
		if(didScroll) {
			didScroll = false;
		}
	}, 100);

	$(function(){
		$('.dropdown').hover(function() {
			$(this).addClass('open');
		},
		function() {
			$(this).removeClass('open');
		});
	});

	//send tab focus to hamburger menu
	
	$('.social-menu > .menuitem:last-child a').keydown(function(e){
		if (e.which == 9){
			e.preventDefault();
			$('.navbar-toggler.collapsed').focus().addClass('hamburger-focus');
		}
	});

	//reset hamburger menu focus
	$('.navbar-toggler.collapsed').on('blur', function(){
		$(this).removeClass('hamburger-focus');
	});
});

//document.addEventListener('touchstart', ontouchstart, {passive: true});