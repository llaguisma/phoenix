//determine Q1, Q2, Q3 or Q4
var date = new Date();
var z = date.getMonth() + 1;

//update theme via url query
var theme = getQueryVariable("theme");

(theme == "spring") ? updateBackground(theme, spring) : null;
(theme == "summer") ? updateBackground(theme, summer) : null;
(theme == "fall") ? updateBackground(theme, fall) : null;
(theme == "winter") ? updateBackground(theme, winter) : null;

//retrieve season quarter
if (!theme) {
	(z >= 3 && z <= 5) ? updateBackground("spring", spring) : null;
	(z >= 6 && z <= 8) ? updateBackground("summer", summer) : null;
	(z >= 9 && z <= 11) ? updateBackground("fall", fall) : null;
	(z == 12 && z < 3) ? updateBackground("winter", winter) : null;
}

(debug) ? console.log("QT month is: "+z) : null;