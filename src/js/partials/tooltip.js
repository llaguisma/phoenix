$( "#first-name, #surname, #email, #phone-number, #datepicker1, #timepicker, #treatments, #a-option, #b-option, #further-notes, #ssl, #find-us, #fb, #tw, #supergui, #first-name-fb, #surname-fb, #customer-notes, #partner1, #partner2, #dark-mode, #season-mode" ).tooltip({
	position: {
		my: "center bottom",
		at: "center top-10",
		collision: "flip",
		using: function( position, feedback ) {
			$( this ).addClass( feedback.vertical )
				.css( position );
		}
	}
});